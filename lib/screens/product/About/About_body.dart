import 'package:flutter/material.dart';
import 'package:furniture_app/screens/product/About/about_component.dart';
import 'package:furniture_app/constants.dart';
import 'package:flutter_svg/svg.dart';
import 'package:furniture_app/screens/product/products_screen.dart';

class AboutBody extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  bool library = true;
  bool services = false;
  bool lottery = false;
  Widget tab;
  @override
  Widget build(BuildContext context) {
    getTab();
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue,
        elevation: 0,
        leading: IconButton(
          padding: EdgeInsets.only(left: kDefaultPadding),
          icon: SvgPicture.asset("assets/icons/back.svg"),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        centerTitle: false,
        title: Text(
          'Kembali'.toUpperCase(),
          style: Theme.of(context).textTheme.bodyText2,
        ),
      ),
      body: Container(
        child: Stack(
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(left: 20, right: 20, top: 10),
              margin: EdgeInsets.only(top: 180),
              constraints: BoxConstraints.expand(height: MediaQuery.of(context).size.height - 219),
              decoration: BoxDecoration(
                  color: darkBlueColor,
                  gradient: LinearGradient(
                    colors: [Colors.blue, lightBlueColor],
                    stops: [2.0, 0.5],
                    begin: const FractionalOffset(0.5,2),
                    end: const FractionalOffset(1, 1),
                    // center: Alignment(0.0, 1),
                  ),
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(0),
                      topRight: Radius.circular(200)
                  ),
                  boxShadow: [
                    new BoxShadow(
                      color: Colors.deepOrange,
                      blurRadius: 20.0,
                    ),
                  ]
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  tab,
                  Container(
                    child: Center(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Container(
                            height: 80,
                            width: 80,
                            child: FloatingActionButton(
                              child: Icon(Icons.library_books),
                              onPressed: () {
                                setState(() {
                                  library = true;
                                  services = false;
                                  lottery = false;
                                  getTab();
                                });
                              },
                              backgroundColor: darkBlueColor,
                            ),
                          ),
                          Container(
                            height: 80,
                            width: 80,
                            child: FloatingActionButton(
                              child: Icon(Icons.monetization_on),
                              onPressed: () {
                                setState(() {
                                  library = false;
                                  services = false;
                                  lottery = true;
                                  getTab();
                                });
                              },
                              backgroundColor: darkBlueColor,
                            ),
                          ),
                          Container(
                            height: 100,
                            width: 80,
                            child: FloatingActionButton(
                              child: Icon(Icons.settings),
                              onPressed: () {
                                setState(() {
                                  library = false;
                                  services = true;
                                  lottery = false;
                                  getTab();
                                });
                              },
                              backgroundColor: darkBlueColor,
                            ),
                          ),
                        ],
                      ),
                    ),
                    height: MediaQuery.of(context).size.height *0.08,
                  )
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(left:40, top: 40),
              height: 160,
              width: MediaQuery.of(context).size.width,
              child: Text("TENTANG\n KAMI", style: pageTitleStyle),
            ),
          ],
        ),
      ),
    );
  }

  void getTab() {
    if (library) {
      tab = cryptoLibrary();
    }
    if (lottery) {
      tab = cryptoLottery();
    }
    if (services) {
      tab = cryptoServices();
    }
  }

  Widget cryptoLottery() {
    return Container(
      height: 100,
    );
  }

  Widget cryptoServices() {
    return Container(
      height: 100,
    );
  }

  Widget cryptoLibrary() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text("Informasi Kontak \nDeveloper", style: tabTitleStyle,),
        Container(
          height: MediaQuery.of(context).size.height * 0.5,
          child: ListView(
            padding: EdgeInsets.only(top: 10),
            children: getLatestArticles(),
          ),
        ),
      ],
    );
  }

  List<Widget> getLatestArticles() {
    List<Widget> articleCards = [];
    for (int i = 0; i < 4; i++) {
      articleCards.add(articleCard());
    }
    return articleCards;
  }

  Widget articleCard() {
    return Container(
      margin: EdgeInsets.only(top: 10),
      height: 120,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Divider(color: darkBlueColor, thickness: 1,),
          Text("WhatsApp\n Developer", style: blogTitleStyle,),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text("September 7, 2019", style: blogDateStyle),
              RaisedButton(
                  color: Colors.white,
                  child: Icon(Icons.chat,color: Colors.blue,),
                  onPressed: () {

                  },
                  shape: RoundedRectangleBorder(borderRadius: new BorderRadius.circular(20.0),)
              )
            ],
          )
        ],
      ),
    );
  }
}
