import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:furniture_app/screens/product/model/Bottom_Global.dart';
import 'package:furniture_app/screens/product/model/Bottom_Job.dart';



class Armada extends StatefulWidget {

  @override
  _ArmadaState createState() => _ArmadaState();
}

class _ArmadaState extends State<Armada> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: backgroundColor,
        child: Column(
          children: <Widget>[
            Stack(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.all(50),
                  constraints: BoxConstraints.expand(height: 300),
                  decoration: BoxDecoration(
                      gradient: new LinearGradient(
                          colors: [lightBlueIsh, Colors.blue],
                          begin: const FractionalOffset(1.0, 1.0),
                          end: const FractionalOffset(1.0, 0.4),
                          stops: [0.0, 1.0],
                          tileMode: TileMode.clamp
                      ),
                      borderRadius: BorderRadius.only(bottomLeft: Radius.circular(30), bottomRight:  Radius.circular(30),)
                  ),
                  child: Container(
                    padding: EdgeInsets.only(top: 10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text('Booking Sekarang', style: titleStyleWhite,)
                      ],
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 90),
                  constraints: BoxConstraints.expand(height:200),
                  child: ListView(
                      padding: EdgeInsets.only(left:40),
                      scrollDirection: Axis.horizontal,
                      children: getRecentJobs()
                  ),
                ),
                Container(
                  height: 610,
                  margin: EdgeInsets.only(top: 100),
                  padding: EdgeInsets.only(top: 180),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(top: 40),
                        child: Text(
                          "Temukan Patnermu",
                          style: titileStyleBlack,
                        ),
                      ),
                      Container(
                        height: 360,
                        child: ListView(
                          children: getJobCategories(),
                        ),
                      )
                    ],
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
  List<String> jobCategories = ["Unggul Ren-Car", "Marno Marni", "Kuproy Car-Rent", "Education", "Finance"];

  Map jobCatToImage = {
    "Unggul Ren-Car" : Image(image:AssetImage("assets/images/2.jpg"),),
    "Engineering" : Icon(Icons.settings, color: lightBlueIsh, size: 40),
    "Health" : Icon(Icons.healing, color: lightBlueIsh, size: 40),
    "Education" : Icon(Icons.search, color: lightBlueIsh, size: 40),
    "Finance" : Icon(Icons.card_membership, color: lightBlueIsh, size: 40),
  };

  Widget getCategoryContainer(String categoryName) {
    return new Container(
      margin: EdgeInsets.only(right: 10, left: 10, bottom: 10,top: 30),
      height: 230,
      width: 180,
      padding: EdgeInsets.all(20),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(10)),
        boxShadow: [
          new BoxShadow(
            color: Colors.grey,
            blurRadius: 10.0,
          ),
        ],
      ),
      child: Column(
        children: <Widget>[
          Text(categoryName, style: titileStyleLighterBlack),
          Container(
            padding: EdgeInsets.only(top:5),
            height: 135,
            width: 180,
            child:  jobCatToImage[categoryName],
            ),
        ],
      ),
    );
  }

  List<Widget> getJobCategories() {
    List<Widget> jobCategoriesCards = [];
    List<Widget> rows = [];
    int i = 0;
    for (String category in jobCategories) {
      if (i < 2) {
        rows.add(getCategoryContainer(category));
        i ++;
      } else {
        i = 0;
        jobCategoriesCards.add(new Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: rows,
        ));
        rows = [];
        rows.add(getCategoryContainer(category));
        i++;
      }
    }
    if (rows.length > 0) {
      jobCategoriesCards.add(new Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: rows,
      ));
    }
    return jobCategoriesCards;
  }

  List<Job> findJobs() {
    List<Job> jobs = [];
    for (int i = 0; i < 10; i++) {
      jobs.add(new Job("Volvo", "Frontend Developer", 20000, "Remote", "Part time", new AssetImage("assets/images/volvo.jpg")));
    }
    return jobs;
  }

  String makeSalaryToK(double salary) {
    String money = "";
    if (salary > 1000) {
      if (salary > 100000000) {
        salary = salary/100000000;
        money = salary.toInt().toString() + "M";
      } else {
        salary = salary/1000;
        money = salary.toInt().toString() + "K";
      }
    } else {
      money = salary.toInt().toString();
    }
    return "\$" + money;
  }

  List<Widget> getRecentJobs() {
    List<Widget> recentJobCards = [];
    List<Job> jobs = findJobs();
    for (Job job in jobs) {
      recentJobCards.add(getJobCard(job));
    }
    return recentJobCards;
  }

  Widget getJobCard(Job job) {
    return Container(
      padding: EdgeInsets.all(10),
      margin: EdgeInsets.only(right: 20, bottom: 30, top: 30),
      height: 150,
      width: 200,
      decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            new BoxShadow(
              color: Colors.grey,
              blurRadius: 20.0,
            ),
          ],
          borderRadius: BorderRadius.all(Radius.circular(15))
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Row(
            children: <Widget>[
              CircleAvatar(
                backgroundImage: job.companyLogo,
              ),
              Text(
                job.jobTitle,
                style: jobCardTitileStyleBlue,
              )
            ],
          ),
          Text(job.companyName + " - " + job.timeRequirement, style: jobCardTitileStyleBlack),
          Text(job.location),
          Text(makeSalaryToK(job.salary), style: salaryStyle)
        ],
      ),
    );
  }
}
