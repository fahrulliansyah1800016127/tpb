import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:furniture_app/constants.dart';
import 'components/Bottom.dart';
import 'package:furniture_app/screens/Profile/Profile_List.dart';
import 'components/body.dart';
import 'components/LinkUp.dart';
import 'package:furniture_app/screens/product/model/Bottom_models.dart';
import 'package:furniture_app/screens/product/About/About_body.dart';
import 'package:furniture_app/screens/Profile/component/List_Profile.dart';
import 'package:furniture_app/screens/product/About/About_body.dart';


class ProductsScreen extends StatefulWidget {

  @override
  _ProductsScreenState createState() => _ProductsScreenState();
}

class _ProductsScreenState extends State<ProductsScreen> with SingleTickerProviderStateMixin {
  int selectedNavBar = 0;
  TabController controller;
  @override
  void initState() {
    controller = new TabController(length: 4, vsync: this);
    super.initState();
  }
  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(),
      backgroundColor: kPrimaryColor,
      body: TabBarView(
          controller: controller,
          children: <Widget>[
            new Body(),
            new Armada(),
            new GaleryPage(),
            new Profile(),

      ]),
      bottomNavigationBar: new Material(
        color : Colors.transparent,
        child : new TabBar(
            controller : controller,
            tabs: <Widget>[
              new Tab(icon: new Icon(Icons.home,color: Colors.orangeAccent),text: 'Home',),
              new Tab(icon: new Icon(Icons.car_rental_outlined,color: Colors.orangeAccent),text: 'Armada',),
              new Tab(icon: new Icon(Icons.photo_album,color: Colors.orangeAccent),text: 'Galery',),
              new Tab(icon: new Icon(Icons.person,color: Colors.orangeAccent),text: 'Profile',),
            ]
        )
    ),

    );
  }

  AppBar buildAppBar() {
    return AppBar(
      elevation: 0,
      centerTitle: false,
      title: Text('MENU'),
      actions: <Widget>[
        IconButton(
          icon: SvgPicture.asset("assets/icons/notification.svg"),
          onPressed: () {},
        ),
        IconButton(
            onPressed: (){
              Navigator.push(
                context,
              MaterialPageRoute(builder: (context) => AboutBody()));
            },
            icon: SvgPicture.asset("assets/icons/chat.svg"))
      ],
    );
  }
}
