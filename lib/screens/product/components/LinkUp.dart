import 'package:url_launcher/url_launcher.dart';
import 'package:flutter/material.dart';

class Link extends StatefulWidget {
  @override
  _LinkState createState() => _LinkState();
}

class _LinkState extends State<Link> {
  void launchWhatsapp({@required number, @required message}) async {
    String url = " whatsapp://send?phone=$number&text=$message";

    await canLaunch(url) ? launch(url) : print(
        "open whatsapp app link or do a snackbar with notification that there is no whatsapp installed");
  }
  @override
  Widget build(BuildContext context){
    return MaterialApp(
      home: Scaffold(
        body: Container(
          child: RaisedButton(
              onPressed:(){
                launchWhatsapp(number: "+6282250016434", message: "Haloo");
              },
              child: Text("klik WhatsApp"),),
        ),
      )
    );
  }

  noSuchMethod(Invocation invocation) => super.noSuchMethod(invocation);
}